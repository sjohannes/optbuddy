# OptBuddy
# Copyright (C) 2018  Johannes Sasongko <sasongko@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.


__all__ = ['DATADIR']

from importlib.util import find_spec
import pathlib

spec = find_spec('optbuddy')
assert spec
assert spec.origin
srcroot = pathlib.Path(spec.origin).parent.parent
if srcroot.name.endswith('.egg'):  # Installed on Windows
    DATADIR = srcroot / 'data'
else:
    parent = srcroot.parent
    if (parent / 'setup.py').exists():  # Not installed
        DATADIR = parent / 'data'
    else:  # Installed on Unix-like
        prefix = parent.parent.parent
        DATADIR = prefix.joinpath('share', 'optbuddy')

assert DATADIR.is_dir()
