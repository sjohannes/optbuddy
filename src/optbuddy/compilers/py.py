# OptBuddy
# Copyright (C) 2019  Johannes Sasongko <sasongko@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.


__all__ = ['compile']


import pathlib
import shlex
import subprocess
import tempfile
from typing import Iterable, Tuple, Union

from ._base import Compiler


INITIAL_CODE = '''def foo(a, b):
    print(a, b)
'''


def compile(
    code: str, flags: str, python: Union[str, Iterable[str], None] = None
) -> Tuple[bool, str, str]:

    if python is None:
        import sys

        python = [sys.executable]
    elif isinstance(python, str):
        python = [python]
    elif not isinstance(python, list):
        python = list(python)

    console = ""
    try:
        with tempfile.NamedTemporaryFile(
            mode='w', prefix='optbuddy-', suffix='.py', encoding='utf-8', delete=False
        ) as f:
            f.write(code)
            path_py = f.name
        res = subprocess.run(
            python + [*shlex.split(flags), '-m', 'dis', path_py],
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE,
        )
        console = res.stderr.decode('utf-8', 'replace')
        res.check_returncode()
        asm = postprocess(res.stdout, path_py).decode('utf-8', 'replace')
        ok = True
    except subprocess.CalledProcessError:
        asm = ""
        ok = False
    finally:
        try:
            pathlib.Path(path_py).unlink()
        except Exception:
            pass
    return ok, asm, console


def postprocess(dump: bytes, tmppath: str) -> bytes:
    tmppath_b = tmppath.encode('utf-8')
    return dump.replace(tmppath_b, b'code.py')


class PyCompiler(Compiler):
    name = "Python"
    gtksource_language = 'python'
    initial_code = INITIAL_CODE
    initial_flags = ''

    def compile(self, code: str, flags: str):
        return compile(code, flags)


compiler_class = PyCompiler
