# OptBuddy
# Copyright (C) 2018  Johannes Sasongko <sasongko@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.


__all__ = ['compile']


import os
import pathlib
import re
import shlex
import subprocess
import tempfile
from typing import Iterable, List, Pattern, Tuple, Union

from ._base import Compiler


INITIAL_CODE = r'''#include <cstddef>
#include <memory>
#include <string>

std::size_t foo(const std::unique_ptr<std::string> &str, int num) {
    auto size = str->size();
    return size + num;
}
'''

REPLACEMENTS: List[Tuple[Union[bytes, Pattern[bytes]], bytes]] = [
    (b' >', b'>'),
    (
        re.compile(
            br'std::map<(.+?), (.+?), std::less<\1>, std::allocator<std::pair<\1 const, \2>>>'
        ),
        br'std::map<\1, \2>',
    ),
    (re.compile(br'std::set<(.+?), std::less<\1>, std::allocator<\1>>'), br'std::set<\1>'),
    (
        b'std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char>>',
        b'std::string',
    ),
    (re.compile(br'std::unique_ptr<(.+?), std::default_delete<\1>>'), br'std::unique_ptr<\1>'),
    (
        re.compile(
            br'std::unordered_map<(.+?), (.+?), std::hash<\1>, std::equal_to<\1>, std::allocator<std::pair<\1 const, \2>>>'
        ),
        br'std::unordered_map<\1, \2>',
    ),
    (
        re.compile(
            br'std::unordered_set<(.+?), std::hash<\1>, std::equal_to<\1>, std::allocator<\1>>'
        ),
        br'std::unordered_set<\1>',
    ),
    (re.compile(br'std::vector<(.+?), std::allocator<\1>>'), br'std::vector<\1>'),
    (
        b'std::__cxx11::basic_string<wchar_t, std::char_traits<wchar_t>, std::allocator<wchar_t>>',
        b'std::wstring',
    ),
]


def compile(
    code: str, flags: str, cxx: Union[str, Iterable[str]] = 'c++', objdump: str = 'objdump'
) -> Tuple[bool, str, str]:

    if isinstance(cxx, str):
        cxx = [cxx]
    elif not isinstance(cxx, list):
        cxx = list(cxx)
    console = ""
    try:
        with tempfile.NamedTemporaryFile(
            mode='w', prefix='optbuddy-', suffix='.cpp', encoding='utf-8', delete=False
        ) as f:
            f.write(code)
            path_cpp = f.name
        path_o = str(pathlib.Path(path_cpp).with_suffix('.o'))
        res = subprocess.run(
            cxx + ['-c', '-g', '-o', path_o, '-pipe', *shlex.split(flags), path_cpp],
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE,
        )
        console += res.stderr.decode('utf-8', 'replace')
        res.check_returncode()
        env = os.environ.copy()
        env['LC_ALL'] = 'C'  # Can't parse translated objdump output
        res = subprocess.run(
            [
                objdump,
                '--demangle=auto',
                '--disassemble-all',  # Strings etc.
                '--disassembler-options=intel',
                '--insn-width=4',
                '--line-numbers',
                '--reloc',  # https://stackoverflow.com/q/34793452
                '--wide',
                path_o,
            ],
            env=env,
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE,
        )
        console += res.stderr.decode('utf-8', 'replace')
        res.check_returncode()
        asm = parse_objdump(res.stdout, path_cpp).decode('utf-8', 'replace')
        ok = True
    except subprocess.CalledProcessError:
        asm = ""
        ok = False
    finally:
        try:
            pathlib.Path(path_cpp).unlink()
        except Exception:  # pragma: no cover
            pass
        try:
            pathlib.Path(path_o).unlink()
        except Exception:
            pass
    return ok, asm, console


def parse_objdump(dump: bytes, tmppath: str) -> bytes:
    tmppath_b = tmppath.encode('utf-8')
    result = dump.split(os.linesep.encode('utf-8'))

    try:
        start_pos = result.index(b'Disassembly of section .text:')
    except ValueError:
        return b''
    end_pos = len(result)
    for i in range(start_pos, end_pos):
        if result[i].startswith(b'Disassembly of section .debug_'):
            end_pos = i
            break
    result = result[start_pos:end_pos]

    for i, line in enumerate(result):
        if line.startswith(tmppath_b):
            line = b"code.cpp" + line[len(tmppath_b) :]
        else:
            for orig, repl in REPLACEMENTS:
                if isinstance(orig, bytes):
                    line = line.replace(orig, repl)
                else:
                    line = orig.sub(repl, line)
        result[i] = line
    return b'\n'.join(result)


class CxxCompiler(Compiler):
    name = "C++"
    gtksource_language = 'cpp'
    initial_code = INITIAL_CODE
    initial_flags = '-O2'

    def compile(self, code: str, flags: str):
        return compile(code, flags)


compiler_class = CxxCompiler
