# OptBuddy
# Copyright (C) 2018  Johannes Sasongko <sasongko@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.


import threading
import warnings

import gi

gi.require_version('Gtk', '3.0')
try:
    gi.require_version('GtkSource', '4')
    GTK_SOURCE_VERSION = 4
except ValueError:
    gi.require_version('GtkSource', '3.0')
    GTK_SOURCE_VERSION = 3

from gi.repository import Gio, GLib, GObject, Gtk, GtkSource

from . import compilers
from .paths import DATADIR


class App(Gtk.Application):
    def __init__(self):
        if GTK_SOURCE_VERSION >= 4:
            GtkSource.init()
        self.window = None
        super().__init__(application_id='com.wordpress.sjohannes.OptBuddy')

    if GTK_SOURCE_VERSION >= 4:

        def __del__(self):
            GtkSource.finalize()

    def do_activate(self) -> None:
        window = self.window
        if not window:
            window = self.window = Window(self)
        window.present()


class Window(Gtk.ApplicationWindow):
    def __init__(self, application: App):
        super().__init__(application=application, title="OptBuddy")
        with warnings.catch_warnings():
            warnings.simplefilter('ignore')
            self.set_wmclass('OptBuddy', "OptBuddy")  # For GNOME Shell
        GObject.type_ensure(GtkSource.View)
        builder = Gtk.Builder.new_from_file(str(DATADIR / 'optbuddy.ui'))
        root = builder.get_object('root')
        self.add(root)
        self.resize(1000, 500)
        root.show_all()

        self.compiler_classes = comps = compilers.get_compiler_classes()
        self.compiler = compiler = comps['cxx']()

        self.update_seq = 0
        code_view = builder.get_object('code_view')
        self.code_buffer = code_buffer = code_view.get_buffer()
        code_buffer.props.text = compiler.initial_code
        self.asm_buffer = builder.get_object('asm_view').get_buffer()
        self.flags_entry = flags_entry = builder.get_object('flags_entry')
        flags_entry.props.text = compiler.initial_flags
        self.console_buffer = builder.get_object('console_view').get_buffer()
        self.update()

        langman = GtkSource.LanguageManager.get_default()
        self.code_buffer.props.language = langman.get_language(compiler.gtksource_language)

        builder.get_object('registers_image').set_from_file(str(DATADIR / 'registers.png'))

        delayed_update = lambda *_: self.update(1)
        code_buffer.connect('changed', delayed_update)
        flags_entry.connect('changed', delayed_update)

        builder.get_object('indent_adjustment').connect(
            'value-changed', lambda a: code_view.set_indent_width(a.props.value)
        )
        builder.get_object('tab_adjustment').connect(
            'value-changed', lambda a: code_view.set_tab_width(a.props.value)
        )
        builder.get_object('tab_check_button').connect(
            'toggled', lambda b: code_view.set_insert_spaces_instead_of_tabs(not b.props.active)
        )

    def update(self, delay: int = 0) -> None:
        seq = self.update_seq + 1
        self.update_seq = seq

        def spawn():
            if seq != self.update_seq:
                return
            flags = self.flags_entry.props.text
            thread = threading.Thread(
                target=lambda: GLib.idle_add(
                    update, *self.compiler.compile(self.code_buffer.props.text, flags)
                )
            )
            thread.start()

        def update(ok, asm, console):
            if seq != self.update_seq:
                return
            self.asm_buffer.props.text = asm
            self.console_buffer.props.text = console

        if delay:
            GLib.timeout_add_seconds(1, spawn)
        else:
            spawn()
