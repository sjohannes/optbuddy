def main():  # pragma: no cover
    from gi.repository import GLib
    from . import ui

    GLib.set_prgname('OptBuddy')
    ui.App().run()
