# OptBuddy
# Copyright (C) 2018  Johannes Sasongko <sasongko@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.


import re


def asm_re(*lines):
    """Convert assembly code into whitespace-insensitive regex"""
    asm = []
    for line in lines:
        line = line.split(None, 1)
        op = line[0]
        if len(line) > 1:
            args = r'\s+' + r',\s*'.join(map(re.escape, re.split(r',\s*', line[1])))
        else:
            args = ''
        asm.append(
            r'(\S[^\n]+\n)*'  # Non-asm lines (those starting with \S)
            + fr'[^\n]+\s{op}{args}\s*\n'
        )
    return ''.join(asm)
