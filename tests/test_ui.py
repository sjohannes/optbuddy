# OptBuddy
# Copyright (C) 2018  Johannes Sasongko <sasongko@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.


from concurrent.futures import Future
import re
from threading import Thread

import gi
import pytest

gi.require_version('Gtk', '3.0')
from gi.repository import GLib, Gtk

from optbuddy.ui import App, Window
from .asm_re import asm_re


class ChainedFuture(Future):
    """A Future where the result() call chains"""

    def __init__(self, timeout=None):
        super().__init__()
        self.timeout = timeout

    def result(self):
        res = super().result(self.timeout)
        while isinstance(res, ChainedFuture):
            res = res.result()
        return res


def grun(cb, delay=0):
    future = ChainedFuture(delay + 0.5)
    f = lambda: future.set_result(cb())
    if delay:
        GLib.timeout_add(delay * 1000, f)
    else:
        GLib.idle_add(f)
    return future


@pytest.fixture
def window():
    future = Future()

    def target():
        window = Window(None)
        window.connect('destroy', lambda _: Gtk.main_quit())
        future.set_result(window)
        Gtk.main()

    thread = Thread(target=target, daemon=True)
    thread.start()
    return future.result()


def test_basic():
    app = App()
    window = Window(app)


def test_asm_changes(window):
    def func1():
        return window.asm_buffer.props.text

    orig_asm = grun(func1, 1).result()

    def func2():
        window.code_buffer.props.text = 'int test(int i) { return i; }'
        window.flags_entry.props.text = '-O2'
        return grun(func3, 2)

    def func3():
        asm = window.asm_buffer.props.text
        window.destroy()
        return asm

    asm = grun(func2).result()
    assert asm != orig_asm
