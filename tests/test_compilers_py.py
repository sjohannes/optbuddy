# OptBuddy
# Copyright (C) 2019  Johannes Sasongko <sasongko@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.


import re

from optbuddy.compilers.py import compile


def test_simple():
    ok, asm, con = compile('a = 1', '')
    print(asm)
    print(con)
    assert ok

    asm = asm.replace('\r\n', '\n')
    asm = re.sub(r' +', ' ', asm)
    assert (
        asm
        == ''' 1 0 LOAD_CONST 0 (1)
 2 STORE_NAME 0 (a)
 4 LOAD_CONST 1 (None)
 6 RETURN_VALUE
'''
    )


def test_syntax_error():
    ok, asm, con = compile('!@#$%', '')
    assert not ok
    assert not asm
    assert con
