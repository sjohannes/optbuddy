# OptBuddy
# Copyright (C) 2018  Johannes Sasongko <sasongko@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.


import platform
import re
from typing import Iterable

import pytest

from optbuddy.compilers.cxx import compile
from .asm_re import asm_re


RE_UNMANGLE = re.compile('^0000000000000000 <(.*)>:$', re.MULTILINE)


def _get_callconv() -> str:
    import platform

    arch = platform.architecture()
    if arch[0] == '32bit':
        return 'cdecl'
    if arch[1] == 'WindowsPE':
        return 'msvc64'
    if arch[1] == 'ELF' or platform.system() == 'Linux':
        return 'sysv64'
    return ''


CALLCONV = _get_callconv()


def test_simple():
    ok, asm, _ = compile('', '')
    assert ok
    assert not asm

    ok, asm, _ = compile('int test(int i) { return i; }', '')
    assert ok
    assert asm


def test_warning():
    ok, asm, con = compile(
        'int test(int i) { return 0; }', '-Wall -Wextra', cxx=['env', 'LC_ALL=C', 'c++']
    )
    assert ok
    assert asm
    assert 'unused parameter' in con


@pytest.mark.skipif(CALLCONV != 'cdecl', reason="wrong platform")
def test_asm_cdecl():
    ok, asm, _ = compile('int test(int i) { return i; }', '-O2')
    assert ok
    print(asm)
    assert re.search(asm_re('mov eax, DWORD PTR [esp+0x4]', 'ret'), asm)


@pytest.mark.skipif(CALLCONV != 'msvc64', reason="wrong platform")
def test_asm_msvc64():
    ok, asm, _ = compile('int test(int i) { return i; }', '-O2')
    assert ok
    print(asm)
    assert re.search(asm_re('mov eax, ecx', 'ret'), asm)


@pytest.mark.skipif(CALLCONV != 'sysv64', reason="wrong platform")
def test_asm_sysv64():
    ok, asm, _ = compile('int test(int i) { return i; }', '-O2')
    assert ok
    print(asm)
    assert re.search(asm_re('mov eax, edi', 'ret'), asm)


def test_invalid_code():
    ok, asm, _ = compile('test', '')
    assert not ok
    assert not asm


def test_invalid_flags():
    ok, asm, _ = compile('int test(int i) { return i; }', '-?!')
    assert not ok
    assert not asm


def test_tools_failure():
    with pytest.raises(FileNotFoundError):
        compile('', '', cxx='#')
    with pytest.raises(FileNotFoundError):
        compile('', '', objdump='#')


def _do_test_unmangle(includes: Iterable[str], decl: str, rettype: str, sig: str) -> None:
    lines = ['#include <%s>' % inc for inc in includes]
    lines.append(decl)
    lines.append('%s %s {}' % (rettype, sig))
    code = '\n'.join(lines)
    ok, asm, con = compile(code, '')
    print(asm)
    print(con)
    assert ok
    assert asm
    m = RE_UNMANGLE.search(asm)
    assert m and m.group(1) == sig


def test_unmangle():
    _do_test_unmangle(
        [], 'struct Test { bool b; char c; int i; };', 'Test', 'test(bool, char, int, Test)'
    )


def test_replacements():
    _do_test_unmangle(
        ['map', 'memory', 'set', 'string', 'vector'],
        '',
        'void',
        'test(std::map<std::unique_ptr<std::string>, std::vector<std::set<std::wstring>>>)',
    )
    _do_test_unmangle(
        ['string', 'unordered_map', 'unordered_set'],
        '',
        'void',
        'test(std::unordered_map<std::string, std::unordered_set<std::wstring>>)',
    )
