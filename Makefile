BLACK = $(PYTHON) -m black
DESTDIR =
PREFIX = /usr/local
PYTEST = $(PYTHON) -m pytest
PYTHON = python3

PYTHONVER != $(PYTHON) -c 'import sysconfig; print(sysconfig.get_python_version())'

all:

check:
	PYTHONPATH=src $(PYTEST)

format:
	$(BLACK) -S -l 99 .

formatcheck:
	$(BLACK) -S -l 99 --check --diff .

install:
	$(PYTHON) setup.py install --root=$(DESTDIR) --prefix=$(PREFIX) -O1

installcheck:
	PYTHONPATH=$(DESTDIR)$(PREFIX)/lib/python$(PYTHONVER)/site-packages $(PYTEST)

.PHONY: all check format formatcheck install
