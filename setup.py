#!/usr/bin/env python3

from glob import glob
import os
import sys

import setuptools


kwargs = {}

pkgname = 'optbuddy'
cwd = os.path.abspath(os.path.dirname(__file__))

if sys.platform == 'win32':
    datadir = 'data'
    docdir = ''
else:
    datadir = f'share/{pkgname}'
    docdir = f'share/doc/{pkgname}'

with open(os.path.join(cwd, 'README.md'), encoding='utf-8') as f:
    kwargs['long_description'] = f.read()

kwargs['data_files'] = [
    (datadir, glob('data/*.png') + glob('data/*.ui')),
    (docdir, ['LICENSE.txt', 'ThirdPartyLicenses.txt']),
]

setuptools.setup(
    name=pkgname,
    version='0.0.0',
    description="Compile your code to assembly and display the result",
    long_description_content_type='text/markdown',
    url=f'https://gitlab.com/sjohannes/{pkgname}',
    author="Johannes Sasongko",
    author_email='sasongko@gmail.com',
    classifiers=[
        'Development Status :: 2 - Pre-Alpha',
        'Environment :: X11 Applications :: GTK',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: GNU General Public License v3 or later (GPLv3+)',
        'Operating System :: POSIX',
        'Programming Language :: Python :: 3.6',
        'Programming Language :: Python :: 3.7',
        'Programming Language :: Python :: 3 :: Only',
        'Topic :: Software Development :: Compilers',
        'Topic :: Software Development :: Disassemblers',
    ],
    keywords='assembly c++',
    packages=setuptools.find_packages('src'),
    package_dir={'': 'src'},
    zip_safe=False,
    entry_points={'gui_scripts': [f'{pkgname}={pkgname}:main']},
    **kwargs,
)
