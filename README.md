# OptBuddy

Compile your code to assembly and display the result.

This is similar to [Compiler Explorer](https://godbolt.org/) but is designed for the desktop (and is not as amazing).

The code is mostly written for and tested with the x86-64 architecture and GNU compiler toolset in mind.
Contributions to make it more platform-independent are welcome.


## Requirements

* Python >=3.6
* PyGObject
* GTK+ 3
* GtkSourceView 3 or 4
* GObject Introspection typelib files for GTK+ 3 and GtkSourceView 3 or 4
* GCC-like C++ compiler
* GNU objdump (doesn't currently work with llvm-objdump)

Installation:

* Setuptools

Development:

* Make
* pytest (for testing)
* coverage.py and pytest-cov (to check test coverage)
* MyPy (for type checking)
* Black (to format the code)


## Installing

    $ python3 setup.py install -O1

For packagers:
You can either use `make install DESTDIR=... PREFIX=...` or use the standard `setup.py` mechanism.


## Testing

To test the code in-place, run `make check`.
To test installed code, run `make installcheck DESTDIR=... PREFIX=...`.

The UI tests are disabled by default because they may segfault if you don't have a GUI environment running.
To re-enable them, run the test harness with `PYTEST_ADDOPTS='-k ""'`.


## Contributing

By contributing to the OptBuddy project, you agree to license your contributions under the GNU General Public License version 3 and later.
